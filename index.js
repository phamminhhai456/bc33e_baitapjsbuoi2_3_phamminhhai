/**Bài 1 */

function tinhLuong() {
  var luongMotNgay = document.getElementById("luongMotNgay").value * 1;
  var soNgayLam = document.getElementById("soNgayLam").value * 1;

  var salary = document.getElementById("salary");
  salary.innerText = luongMotNgay * soNgayLam;
}

/** Bài 2 */
function tinhTrungBinh() {
  var num1 = parseInt(document.getElementById("num1").value);
  var num2 = parseInt(document.getElementById("num2").value);
  var num3 = parseInt(document.getElementById("num3").value);
  var num4 = parseInt(document.getElementById("num4").value);
  var num5 = parseInt(document.getElementById("num5").value);

  var average = document.getElementById("average");

  var trungBinh = (num1 + num2 + num3 + num4 + num5) / 5;

  average.innerText = trungBinh;
}

/** Bài 3 */

function quyDoiTien() {
  var tienUSD = parseInt(document.getElementById("tienUSD").value);

  const motUSD = 23500;

  var currency = document.getElementById("currency");

  var tienDaQuyDoi = tienUSD * motUSD;

  currency.innerText = new Intl.NumberFormat("vn-VN").format(tienDaQuyDoi);
}

/** Bài 4 */

function tinhCVDT() {
  var chieuDai = parseInt(document.getElementById("chieuDai").value);
  var chieuRong = parseInt(document.getElementById("chieuRong").value);

  var chuViEl = document.getElementById("chuVi");
  var dienTichEl = document.getElementById("dienTich");

  var chuVi = (chieuDai + chieuRong) * 2;
  var dienTich = chieuDai * chieuRong;

  chuViEl.innerText = chuVi;
  dienTichEl.innerText = dienTich;
}

/** Bài 5 */

function tinhTong() {
  var soNhap = parseInt(document.getElementById("soNhap").value);
  var sumEl = document.getElementById("sum");

  var soHangDonVi = soNhap % 10;
  var soHangChuc = Math.floor(soNhap / 10);

  var tong = soHangDonVi + soHangChuc;

  sumEl.innerText = tong;
}
